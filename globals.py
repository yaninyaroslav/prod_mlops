# Data paths
# ## For MacOs
# RAW_TRAIN_DATA_PATH = "/Users/user/PycharmProjects/prod_mlops/data/raw/features.csv"
# RAW_PREDICTION_DATA_PATH = "/Users/user/PycharmProjects/prod_mlops/data/raw/features_test.csv"
#
# INTERIM_TRAIN_DATA_PATH = "/Users/user/PycharmProjects/prod_mlops/data/interim/train_data.csv"
# INTERIM_PREDICTION_DATA_PATH = "/Users/user/PycharmProjects/prod_mlops/data/interim/prediction_data.csv"
# PROCESSED_Y_DATA_PATH = "/Users/user/PycharmProjects/prod_mlops/data/processed/y_train.csv"
#
# EXTERNAL_TRAIN_DATA_PATH = "/Users/user/PycharmProjects/prod_mlops/data/external/train_feat.csv"
# EXTERNAL_PREDICTION_DATA_PATH = "/Users/user/PycharmProjects/prod_mlops/data/external/prediction_feat.csv"
#
# PROCESSED_DATA_TRAIN_PATH = "/Users/user/PycharmProjects/prod_mlops/data/processed/train.csv"
# PROCESSED_DATA_PREDICTION_PATH = "/Users/user/PycharmProjects/prod_mlops/data/processed/prediction.csv"
#
# SAVE_MODELS_PATH = "/Users/user/PycharmProjects/prod_mlops/models/rfc.pkl"
# SAVE_PREDICTION_PATH = "/Users/user/PycharmProjects/prod_mlops/reports/answer.csv"

## For Windows
RAW_TRAIN_DATA_PATH = r"data\raw\features.csv"
RAW_PREDICTION_DATA_PATH = r"data\raw\features_test.csv"

INTERIM_TRAIN_DATA_PATH = r"data\interim\train_data.csv"
INTERIM_PREDICTION_DATA_PATH = r"data\interim\prediction_data.csv"

PROCESSED_Y_DATA_PATH = r"data\processed\y_train.csv"
PROCESSED_DATA_TRAIN_PATH = r"data\processed\train.csv"
PROCESSED_DATA_PREDICTION_PATH = r"data\processed\prediction.csv"

EXTERNAL_TRAIN_DATA_PATH = r"data\external\train_feat.csv"
EXTERNAL_PREDICTION_DATA_PATH = r"data\external\prediction_feat.csv"

SAVE_MODELS_PATH = r"models\rfc.pkl"
SAVE_PREDICTION_PATH = r"reports\answer.csv"

# Column names for datasets
COLUMNS_TRAIN = [
    "match_id", "start_time", "lobby_type", "r1_hero", "r1_level", "r1_xp", "r1_gold",
    "r1_lh", "r1_kills", "r1_deaths", "r1_items", "r2_hero", "r2_level", "r2_xp",
    "r2_gold", "r2_lh", "r2_kills", "r2_deaths", "r2_items", "r3_hero", "r3_level",
    "r3_xp", "r3_gold", "r3_lh", "r3_kills", "r3_deaths", "r3_items", "r4_hero",
    "r4_level", "r4_xp", "r4_gold", "r4_lh", "r4_kills", "r4_deaths", "r4_items",
    "r5_hero", "r5_level", "r5_xp", "r5_gold", "r5_lh", "r5_kills", "r5_deaths",
    "r5_items", "d1_hero", "d1_level", "d1_xp", "d1_gold", "d1_lh", "d1_kills",
    "d1_deaths", "d1_items", "d2_hero", "d2_level", "d2_xp", "d2_gold", "d2_lh",
    "d2_kills", "d2_deaths", "d2_items", "d3_hero", "d3_level", "d3_xp", "d3_gold",
    "d3_lh", "d3_kills", "d3_deaths", "d3_items", "d4_hero", "d4_level", "d4_xp", "d4_gold",
    "d4_lh", "d4_kills", "d4_deaths", "d4_items", "d5_hero", "d5_level", "d5_xp", "d5_gold",
    "d5_lh", "d5_kills", "d5_deaths", "d5_items", "first_blood_time", "first_blood_team",
    "first_blood_player1", "first_blood_player2", "radiant_bottle_time", "radiant_courier_time",
    "radiant_flying_courier_time", "radiant_tpscroll_count", "radiant_boots_count",
    "radiant_ward_observer_count", "radiant_ward_sentry_count", "radiant_first_ward_time",
    "dire_bottle_time", "dire_courier_time", "dire_flying_courier_time", "dire_tpscroll_count",
    "dire_boots_count", "dire_ward_observer_count", "dire_ward_sentry_count",
    "dire_first_ward_time", "duration", "radiant_win", "tower_status_radiant", "tower_status_dire",
    "barracks_status_radiant", "barracks_status_dire"
]

COLUMNS_PREDICT = [
    "match_id", "start_time", "lobby_type", "r1_hero", "r1_level", "r1_xp", "r1_gold", "r1_lh", "r1_kills", "r1_deaths",
    "r1_items", "r2_hero", "r2_level", "r2_xp", "r2_gold", "r2_lh", "r2_kills", "r2_deaths", "r2_items", "r3_hero",
    "r3_level", "r3_xp", "r3_gold", "r3_lh", "r3_kills", "r3_deaths", "r3_items", "r4_hero", "r4_level", "r4_xp",
    "r4_gold", "r4_lh", "r4_kills", "r4_deaths", "r4_items", "r5_hero", "r5_level", "r5_xp", "r5_gold", "r5_lh",
    "r5_kills", "r5_deaths", "r5_items", "d1_hero", "d1_level", "d1_xp", "d1_gold", "d1_lh", "d1_kills", "d1_deaths",
    "d1_items", "d2_hero", "d2_level", "d2_xp", "d2_gold", "d2_lh", "d2_kills", "d2_deaths", "d2_items", "d3_hero",
    "d3_level", "d3_xp", "d3_gold", "d3_lh", "d3_kills", "d3_deaths", "d3_items", "d4_hero", "d4_level", "d4_xp",
    "d4_gold", "d4_lh", "d4_kills", "d4_deaths", "d4_items", "d5_hero", "d5_level", "d5_xp", "d5_gold", "d5_lh",
    "d5_kills", "d5_deaths", "d5_items", "first_blood_time", "first_blood_team", "first_blood_player1",
    "first_blood_player2", "radiant_bottle_time", "radiant_courier_time", "radiant_flying_courier_time",
    "radiant_tpscroll_count", "radiant_boots_count", "radiant_ward_observer_count", "radiant_ward_sentry_count",
    "radiant_first_ward_time", "dire_bottle_time", "dire_courier_time", "dire_flying_courier_time",
    "dire_tpscroll_count", "dire_boots_count", "dire_ward_observer_count", "dire_ward_sentry_count",
    "dire_first_ward_time"

]

COLUMNS_TRAIN_PREDICTION_EXTERNAL = [
    'Unnamed: 0', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
    '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22',
    '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34',
    '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46',
    '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58',
    '59', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '70',
    '71', '72', '73', '74', '75', '76', '77', '78', '79', '80', '81', '82',
    '83', '84', '85', '86', '87', '88', '89', '90', '91', '92', '93', '94',
    '95', '96', '97', '98', '99', '100', '101', '102', '103', '104', '105',
    '106', '107', '108', '109', '110', '111'
]

COLUMNS_TRAIN_PREDICTION_INTERIM = [
    'Unnamed: 0', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
    '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22',
    '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34',
    '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46',
    '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58',
    '59', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '70',
    '71', '72', '73', '74', '75', '76', '77', '78', '79', '80', '81', '82',
    '83', '84', '85', '86', '87', '88', '89', '90'
]

COLUMNS_TRAIN_PREDICTION_PROCESSED = [
    "Unnamed: 0_x", "0_x", "1_x", "2_x", "3_x", "4_x", "5_x", "6_x", "7_x", "8_x", "9_x", "10_x", "11_x", "12_x",
    "13_x", "14_x", "15_x", "16_x", "17_x", "18_x", "19_x", "20_x", "21_x", "22_x", "23_x", "24_x", "25_x", "26_x",
    "27_x", "28_x", "29_x", "30_x", "31_x", "32_x", "33_x", "34_x", "35_x", "36_x", "37_x", "38_x", "39_x", "40_x",
    "41_x", "42_x", "43_x", "44_x", "45_x", "46_x", "47_x", "48_x", "49_x", "50_x", "51_x", "52_x", "53_x", "54_x",
    "55_x", "56_x", "57_x", "58_x", "59_x", "60_x", "61_x", "62_x", "63_x", "64_x", "65_x", "66_x", "67_x", "68_x",
    "69_x", "70_x", "71_x", "72_x", "73_x", "74_x", "75_x", "76_x", "77_x", "78_x", "79_x", "80_x", "81_x", "82_x",
    "83_x", "84_x", "85_x", "86_x", "87_x", "88_x", "89_x", "90_x", '91', '92', '93', '94', '95', '96', '97', '98',
    '99',
    '100', '101', '102', '103', '104', '105', '106', '107', '108', '109', '110', '111', "Unnamed: 0_y", "0_y",
    "1_y", "2_y", "3_y", "4_y", "5_y", "6_y", "7_y", "8_y", "9_y", "10_y", "11_y", "12_y", "13_y", "14_y", "15_y",
    "16_y", "17_y", "18_y", "19_y", "20_y", "21_y", "22_y", "23_y", "24_y", "25_y", "26_y", "27_y", "28_y", "29_y",
    "30_y", "31_y", "32_y", "33_y", "34_y", "35_y", "36_y", "37_y", "38_y", "39_y", "40_y", "41_y", "42_y", "43_y",
    "44_y", "45_y", "46_y", "47_y", "48_y", "49_y", "50_y", "51_y", "52_y", "53_y", "54_y", "55_y", "56_y", "57_y",
    "58_y", "59_y", "60_y", "61_y", "62_y", "63_y", "64_y", "65_y", "66_y", "67_y", "68_y", "69_y", "70_y", "71_y",
    "72_y", "73_y", "74_y", "75_y", "76_y", "77_y", "78_y", "79_y", "80_y", "81_y", "82_y", "83_y", "84_y", "85_y",
    "86_y", "87_y", "88_y", "89_y", "90_y"
]
# 205
print(len(COLUMNS_TRAIN_PREDICTION_PROCESSED))
