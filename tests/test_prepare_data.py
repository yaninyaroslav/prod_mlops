import os
import pytest
import great_expectations as ge
import pandas as pd

from src import prepare_data
from click.testing import CliRunner
from globals import RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH, \
    INTERIM_TRAIN_DATA_PATH, INTERIM_PREDICTION_DATA_PATH, PROCESSED_Y_DATA_PATH, \
    COLUMNS_TRAIN_PREDICTION_INTERIM

runner = CliRunner()


@pytest.fixture(scope='class', autouse=True)
def clean_prepared_data():
    print("Starting TestClassPrepareData")
    yield "resource"
    # Clean data
    print("Clean data after test for TestClassPrepareData")
    os.remove(PROCESSED_Y_DATA_PATH)
    os.remove(INTERIM_TRAIN_DATA_PATH)
    os.remove(INTERIM_PREDICTION_DATA_PATH)


class TestClassPrepareData:

    @staticmethod
    def test_cli_command():
        result = runner.invoke(prepare_data, [RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH,
                                              PROCESSED_Y_DATA_PATH, INTERIM_TRAIN_DATA_PATH,
                                              INTERIM_PREDICTION_DATA_PATH])
        assert result.exit_code == 0

    @staticmethod
    def test_processed_y_data():
        _ = runner.invoke(prepare_data, [RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH,
                                         PROCESSED_Y_DATA_PATH, INTERIM_TRAIN_DATA_PATH,
                                         INTERIM_PREDICTION_DATA_PATH])
        y_data = pd.read_csv(PROCESSED_Y_DATA_PATH)
        df_ge = ge.from_pandas(y_data)
        assert df_ge.expect_table_columns_to_match_ordered_list(column_list=["radiant_win"]).success is True

    @staticmethod
    def test_interim_train_data():
        _ = runner.invoke(prepare_data, [RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH,
                                         PROCESSED_Y_DATA_PATH, INTERIM_TRAIN_DATA_PATH,
                                         INTERIM_PREDICTION_DATA_PATH])
        train_data = pd.read_csv(INTERIM_TRAIN_DATA_PATH)
        df_ge = ge.from_pandas(train_data)
        assert df_ge.expect_table_columns_to_match_ordered_list(column_list=COLUMNS_TRAIN_PREDICTION_INTERIM).success \
               is True

    @staticmethod
    def test_interim_prediction_data():
        _ = runner.invoke(prepare_data, [RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH,
                                         PROCESSED_Y_DATA_PATH, INTERIM_TRAIN_DATA_PATH,
                                         INTERIM_PREDICTION_DATA_PATH])
        prediction_data = pd.read_csv(INTERIM_PREDICTION_DATA_PATH)
        df_ge = ge.from_pandas(prediction_data)
        assert df_ge.expect_table_columns_to_match_ordered_list(column_list=COLUMNS_TRAIN_PREDICTION_INTERIM).success \
               is True
