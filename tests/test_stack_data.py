import os
import pytest
import pandas as pd
import great_expectations as ge
from click.testing import CliRunner
from src import stack_data, prepare_data, new_features
from globals import INTERIM_TRAIN_DATA_PATH, INTERIM_PREDICTION_DATA_PATH, EXTERNAL_TRAIN_DATA_PATH, \
    EXTERNAL_PREDICTION_DATA_PATH, PROCESSED_DATA_TRAIN_PATH, PROCESSED_DATA_PREDICTION_PATH, RAW_TRAIN_DATA_PATH, \
    PROCESSED_Y_DATA_PATH, RAW_PREDICTION_DATA_PATH, COLUMNS_TRAIN_PREDICTION_PROCESSED

runner = CliRunner()


@pytest.fixture(scope='class', autouse=True)
def clean_prepared_data():
    print("Starting TestClassStackData")
    runner.invoke(prepare_data, [RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH,
                                 PROCESSED_Y_DATA_PATH, INTERIM_TRAIN_DATA_PATH,
                                 INTERIM_PREDICTION_DATA_PATH])
    runner.invoke(new_features, [RAW_TRAIN_DATA_PATH,
                                 RAW_PREDICTION_DATA_PATH,
                                 EXTERNAL_TRAIN_DATA_PATH,
                                 EXTERNAL_PREDICTION_DATA_PATH])
    yield "resource"
    # Clean data
    print("Clean data after test for TestClassStackData")
    os.remove(PROCESSED_Y_DATA_PATH)
    os.remove(INTERIM_TRAIN_DATA_PATH)
    os.remove(INTERIM_PREDICTION_DATA_PATH)
    os.remove(EXTERNAL_TRAIN_DATA_PATH)
    os.remove(EXTERNAL_PREDICTION_DATA_PATH)
    os.remove(PROCESSED_DATA_TRAIN_PATH)
    os.remove(PROCESSED_DATA_PREDICTION_PATH)


class TestClassStackData:

    @staticmethod
    def test_cli_command():
        result = runner.invoke(stack_data, [EXTERNAL_TRAIN_DATA_PATH,
                                            EXTERNAL_PREDICTION_DATA_PATH,
                                            INTERIM_TRAIN_DATA_PATH,
                                            INTERIM_PREDICTION_DATA_PATH,
                                            PROCESSED_DATA_TRAIN_PATH,
                                            PROCESSED_DATA_PREDICTION_PATH])
        assert result.exit_code == 0

    @staticmethod
    def test_processed_train_data():
        runner.invoke(stack_data, [EXTERNAL_TRAIN_DATA_PATH,
                                   EXTERNAL_PREDICTION_DATA_PATH,
                                   INTERIM_TRAIN_DATA_PATH,
                                   INTERIM_PREDICTION_DATA_PATH,
                                   PROCESSED_DATA_TRAIN_PATH,
                                   PROCESSED_DATA_PREDICTION_PATH])
        train_data = pd.read_csv(PROCESSED_DATA_TRAIN_PATH)
        df_ge = ge.from_pandas(train_data)
        assert df_ge.expect_table_columns_to_match_ordered_list(
            column_list=COLUMNS_TRAIN_PREDICTION_PROCESSED).success is True

    @staticmethod
    def test_processed_prediction_data():
        runner.invoke(stack_data, [EXTERNAL_TRAIN_DATA_PATH,
                                   EXTERNAL_PREDICTION_DATA_PATH,
                                   INTERIM_TRAIN_DATA_PATH,
                                   INTERIM_PREDICTION_DATA_PATH,
                                   PROCESSED_DATA_TRAIN_PATH,
                                   PROCESSED_DATA_PREDICTION_PATH])
        y_data = pd.read_csv(PROCESSED_DATA_PREDICTION_PATH)
        df_ge = ge.from_pandas(y_data)
        assert df_ge.expect_table_columns_to_match_ordered_list(column_list=COLUMNS_TRAIN_PREDICTION_PROCESSED).success \
               is True
