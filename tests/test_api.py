import requests


def test_fast_api():
    url = "http://localhost:8003/invocations"
    file_name = r"C:\Users\user\PycharmProjects\prod_mlops\data\processed\example_data.csv"
    files = {'file': ("example_data.csv", open(file_name, "rb"),
                      'text/csv')}
    send_data = requests.post(url, files=files)
    assert send_data.status_code == 200
    assert "[0,0,1,1]" == send_data.text
