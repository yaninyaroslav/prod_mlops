import os
import pytest
from click.testing import CliRunner
from src import train_rfc, prepare_data, new_features, stack_data
from globals import INTERIM_TRAIN_DATA_PATH, INTERIM_PREDICTION_DATA_PATH, EXTERNAL_TRAIN_DATA_PATH, \
    EXTERNAL_PREDICTION_DATA_PATH, PROCESSED_DATA_TRAIN_PATH, PROCESSED_DATA_PREDICTION_PATH, RAW_TRAIN_DATA_PATH, \
    PROCESSED_Y_DATA_PATH, RAW_PREDICTION_DATA_PATH, SAVE_MODELS_PATH

runner = CliRunner()


@pytest.fixture(scope='class', autouse=True)
def clean_prepared_data():
    print("Starting TestClassTrainRandomForestClassifier")
    runner.invoke(prepare_data, [RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH,
                                 PROCESSED_Y_DATA_PATH, INTERIM_TRAIN_DATA_PATH,
                                 INTERIM_PREDICTION_DATA_PATH])
    runner.invoke(new_features, [RAW_TRAIN_DATA_PATH,
                                 RAW_PREDICTION_DATA_PATH,
                                 EXTERNAL_TRAIN_DATA_PATH,
                                 EXTERNAL_PREDICTION_DATA_PATH])
    runner.invoke(stack_data, [EXTERNAL_TRAIN_DATA_PATH,
                               EXTERNAL_PREDICTION_DATA_PATH,
                               INTERIM_TRAIN_DATA_PATH,
                               INTERIM_PREDICTION_DATA_PATH,
                               PROCESSED_DATA_TRAIN_PATH,
                               PROCESSED_DATA_PREDICTION_PATH])
    yield "resource"
    # Clean data
    print("Clean data after test for TestClassTrainRandomForestClassifier")
    os.remove(PROCESSED_Y_DATA_PATH)
    os.remove(INTERIM_TRAIN_DATA_PATH)
    os.remove(INTERIM_PREDICTION_DATA_PATH)
    os.remove(EXTERNAL_TRAIN_DATA_PATH)
    os.remove(EXTERNAL_PREDICTION_DATA_PATH)
    os.remove(PROCESSED_DATA_TRAIN_PATH)
    os.remove(PROCESSED_DATA_PREDICTION_PATH)
    os.remove(SAVE_MODELS_PATH)


class TestClassTrainRandomForestClassifier:

    @staticmethod
    def test_cli_command():
        result = runner.invoke(train_rfc, [PROCESSED_DATA_TRAIN_PATH,
                                           PROCESSED_Y_DATA_PATH,
                                           SAVE_MODELS_PATH])
        assert result.exit_code == 0
