import os
import pytest
import pandas as pd
import great_expectations as ge

from src import new_features
from click.testing import CliRunner
from globals import RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH, \
    EXTERNAL_TRAIN_DATA_PATH, EXTERNAL_PREDICTION_DATA_PATH, COLUMNS_TRAIN_PREDICTION_EXTERNAL

runner = CliRunner()


@pytest.fixture(scope='class', autouse=True)
def clean_external_data():
    print("Starting TestClassExternalData")
    yield "resource"
    # Clean data
    print("Clean data after test for TestClassExternalData")
    os.remove(EXTERNAL_TRAIN_DATA_PATH)
    os.remove(EXTERNAL_PREDICTION_DATA_PATH)


class TestClassExternalData:

    @staticmethod
    def test_cli_command():
        result = runner.invoke(new_features, [RAW_TRAIN_DATA_PATH,
                                              RAW_PREDICTION_DATA_PATH,
                                              EXTERNAL_TRAIN_DATA_PATH,
                                              EXTERNAL_PREDICTION_DATA_PATH])
        assert result.exit_code == 0

    @staticmethod
    def test_columns_external_train_data():
        _ = runner.invoke(new_features, [RAW_TRAIN_DATA_PATH,
                                         RAW_PREDICTION_DATA_PATH,
                                         EXTERNAL_TRAIN_DATA_PATH,
                                         EXTERNAL_PREDICTION_DATA_PATH])
        external_data = pd.read_csv(EXTERNAL_TRAIN_DATA_PATH)
        df_ge = ge.from_pandas(external_data)
        print(df_ge.columns)
        assert df_ge.expect_table_columns_to_match_ordered_list(column_list=COLUMNS_TRAIN_PREDICTION_EXTERNAL).success \
               is True

    @staticmethod
    def test_columns_external_prediction_data():
        external_data = pd.read_csv(EXTERNAL_PREDICTION_DATA_PATH)
        df_ge = ge.from_pandas(external_data)
        assert df_ge.expect_table_columns_to_match_ordered_list(column_list=COLUMNS_TRAIN_PREDICTION_EXTERNAL).success \
               is True
