import pandas as pd
import great_expectations as ge

from globals import RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH, \
    COLUMNS_TRAIN, COLUMNS_PREDICT


def test_columns_train_data():
    train_data = pd.read_csv(RAW_TRAIN_DATA_PATH)
    df_ge = ge.from_pandas(train_data)
    assert df_ge.expect_table_columns_to_match_ordered_list(column_list=COLUMNS_TRAIN).success is True


def test_columns_prediction_data():
    prediction_data = pd.read_csv(RAW_PREDICTION_DATA_PATH)
    df_ge = ge.from_pandas(prediction_data)
    assert df_ge.expect_table_columns_to_match_ordered_list(column_list=COLUMNS_PREDICT).success is True
