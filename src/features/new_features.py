import click
import numpy as np
import pandas as pd
# from src.main_command import main_command


# @main_command.command()
@click.command()
@click.argument("input_path_train", type=click.Path(exists=True))
@click.argument("input_path_prediction", type=click.Path())
@click.argument("output_path_train", type=click.Path())
@click.argument("output_path_prediction", type=click.Path())
def new_features(input_path_train: str, input_path_prediction: str,
                 output_path_train: str, output_path_prediction: str):
    """ Function prepare train and predict data: add info about heroes
        :param input_path_train: Path to read original DataFrame with all listings
        :param input_path_prediction: Path to read prediction DataFrame with all listings
        :param output_path_prediction: Path to save prediction data with new features
        :param output_path_train: Path to save train data with new features
    :return:
    """
    print("new_features")
    train_data = pd.read_csv(input_path_train, index_col='match_id')
    prediction_data = pd.read_csv(input_path_prediction, index_col='match_id')
    print(train_data.shape, "train_data")
    print(prediction_data.shape, "prediction_data")

    num_heroes = train_data['d2_hero'].max()

    new_feat_train = np.zeros((train_data.shape[0], num_heroes))
    new_feat_prediction = np.zeros((prediction_data.shape[0], num_heroes))

    for i, match_id in enumerate(train_data.index):
        for p in range(5):
            new_feat_train[i, train_data.loc[match_id, 'r%d_hero' % (p + 1)] - 1] = 1
            new_feat_train[i, train_data.loc[match_id, 'd%d_hero' % (p + 1)] - 1] = -1

    for i, match_id in enumerate(prediction_data.index):
        for p in range(5):
            new_feat_prediction[i, prediction_data.loc[match_id, 'r%d_hero' % (p + 1)] - 1] = 1
            new_feat_prediction[i, prediction_data.loc[match_id, 'd%d_hero' % (p + 1)] - 1] = -1

    pd.DataFrame(new_feat_train).to_csv(output_path_train)
    pd.DataFrame(new_feat_prediction).to_csv(output_path_prediction)
    print(new_feat_train.shape, "new_feat_train")
    print(new_feat_prediction.shape, "new_feat_prediction")


if __name__ == "__main__":
    new_features()
