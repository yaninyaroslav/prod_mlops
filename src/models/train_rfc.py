import os
import click
import mlflow
import pandas as pd
import joblib as jb
from mlflow.models.signature import infer_signature
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from dotenv import load_dotenv

load_dotenv()

remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://localhost:9000"
os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"


@click.command()
@click.argument("input_path_x", type=click.Path(exists=True))
@click.argument("input_path_y", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def train_rfc(input_path_x: str, input_path_y: str, output_path: str):
    """ Function for find the best parameters for estimator RandomForestClassifier
    :param input_path_x: Path to read prepared x-data for train model
    :param input_path_y: Path to read prepared y-data for train model
    :param output_path: Path to save best model
    :return:
    """
    with mlflow.start_run():
        mlflow.get_artifact_uri()
        print(mlflow.get_artifact_uri())

        x = pd.read_csv(input_path_x)
        y = pd.read_csv(input_path_y)

        parameters = {
            'n_estimators': [50],
            'criterion': ["entropy"],
            'max_depth': [1],
            'min_samples_split': [3],
            'min_samples_leaf': [4],
            'min_weight_fraction_leaf': [0],
            'max_features': ['sqrt'],
            'min_impurity_decrease': [0],
            'bootstrap': [True],
            'oob_score': [False],
            'n_jobs': [2],
            'random_state': [257],
            'verbose': [0],
            'warm_start': [False],
            'class_weight': ['balanced'],
            'ccp_alpha': [0],
            'max_samples': [None]
        }
        rfc = RandomForestClassifier()
        clf = GridSearchCV(rfc, parameters, cv=3, scoring='roc_auc')
        clf.fit(x, y.values.ravel())
        jb.dump(clf, output_path)
        signature = infer_signature(x, y)

        mlflow.log_params(parameters)
        mlflow.log_metrics({"Mean cross-validate roc-auc": clf.best_score_})
        mlflow.sklearn.log_model(sk_model=clf,
                                 artifact_path='model',
                                 registered_model_name='RandomForestClassifier',
                                 signature=signature)


if __name__ == "__main__":
    train_rfc()
