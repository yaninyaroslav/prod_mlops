import click
import pandas as pd


@click.command()
@click.argument("input_clean_path_train", type=click.Path(exists=True))
@click.argument("input_clean_path_prediction", type=click.Path(exists=True))
@click.argument("input_feature_path_train", type=click.Path(exists=True))
@click.argument("input_feature_path_prediction", type=click.Path(exists=True))
@click.argument("output_path_train", type=click.Path())
@click.argument("output_path_prediction", type=click.Path())
def stack_data(input_clean_path_train: str, input_clean_path_prediction: str,
               input_feature_path_train: str, input_feature_path_prediction: str,
               output_path_train: str, output_path_prediction: str):
    """ Function prepare train and predict data: stacking clear data and new features
    :param output_path_prediction:
    :param output_path_train:
    :param input_clean_path_train:
    :param input_feature_path_prediction:
    :param input_feature_path_train:
    :param input_clean_path_prediction:
    """
    print("stack_data")
    train_feat = pd.read_csv(input_clean_path_train)
    print(train_feat.shape, "train_feat")
    train_prep = pd.read_csv(input_feature_path_train)
    print(train_prep.shape, "train_prep")
    train = pd.merge(train_feat, train_prep, left_index=True, right_index=True)
    train.to_csv(output_path_train, index=False)
    print(train.shape, "train")

    prediction_feat = pd.read_csv(input_clean_path_prediction)
    print(prediction_feat.shape, "prediction_feat")
    prediction_prep = pd.read_csv(input_feature_path_prediction)
    print(prediction_prep.shape, "prediction_prep")
    test = pd.merge(prediction_feat, prediction_prep, left_index=True, right_index=True)
    test.to_csv(output_path_prediction, index=False)
    print(test.shape, "test")


if __name__ == "__main__":
    stack_data()
