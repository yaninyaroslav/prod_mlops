import click
import joblib as jb
import pandas as pd


@click.command()
@click.argument("model_path", type=click.Path(exists=True))
@click.argument("predict_data_path", type=click.Path(exists=True))
@click.argument("answer_path", type=click.Path())
def predict_rfc(model_path: str, predict_data_path: str, answer_path: str):
    """ Function for download model and make prediction
    :param model_path: Path to read prepared model
    :param predict_data_path: Path to read prepared data for predict
    :param answer_path: Path to save predicted values
    :return:
    """
    print("predict_rfc")
    predict_data = pd.read_csv(predict_data_path)
    loaded_rfc = jb.load(open(model_path, 'rb'))
    predict_data = loaded_rfc.predict(predict_data)
    predict_data.tofile(answer_path, sep=',')


if __name__ == "__main__":
    predict_rfc()
