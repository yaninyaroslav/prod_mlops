import click
import pandas as pd
from sklearn.preprocessing import StandardScaler
# from src.main_command import main_command


# @main_command.command()
@click.command()
@click.argument("input_path_train", type=click.Path())
@click.argument("input_path_prediction", type=click.Path())
@click.argument("output_path_y", type=click.Path())
@click.argument("output_path_train", type=click.Path())
@click.argument("output_path_prediction", type=click.Path())
def prepare_data(input_path_train, input_path_prediction, output_path_y,
                 output_path_train, output_path_prediction):
    """ Function prepare train and predict data: drop unused columns, using standard scalier
        :param input_path_train: Path to read original DataFrame with all listings
        :param input_path_prediction: Path to read prediction DataFrame with all listings
        :param output_path_y: Path to save y-values DataFrame
        :param output_path_prediction: Path to save prepared prediction data
        :param output_path_train: Path to save prepared train data
    """
    print("prepare_data")
    print(input_path_train)
    train_data = pd.read_csv(input_path_train, index_col='match_id')
    print(train_data.shape, "train_data")
    prediction_data = pd.read_csv(input_path_prediction, index_col='match_id')
    print(prediction_data.shape, "prediction_data")
    y_train = train_data['radiant_win']
    print(y_train.shape, "y_train")

    train_data.fillna(0, inplace=True)
    prediction_data.fillna(0, inplace=True)

    train_data.drop(['radiant_win', 'duration', 'tower_status_radiant', 'tower_status_dire',
                     'barracks_status_radiant', 'barracks_status_dire', 'lobby_type', 'r1_hero',
                     'r2_hero', 'r3_hero', 'r4_hero', 'r5_hero', 'd1_hero', 'd2_hero', 'd3_hero',
                     'd4_hero', 'd5_hero'], inplace=True, axis=1)

    prediction_data.drop(['lobby_type', 'r1_hero',
                          'r2_hero', 'r3_hero', 'r4_hero', 'r5_hero', 'd1_hero', 'd2_hero', 'd3_hero',
                          'd4_hero', 'd5_hero'], inplace=True, axis=1)

    scaler = StandardScaler()
    train_data = scaler.fit_transform(train_data)
    prediction_data = scaler.fit_transform(prediction_data)

    y_train.to_csv(output_path_y, index=False)
    pd.DataFrame(train_data).to_csv(output_path_train)
    pd.DataFrame(prediction_data).to_csv(output_path_prediction)
    print(train_data.shape, "train_data")
    print(prediction_data.shape, "prediction_data")


if __name__ == "__main__":
    prepare_data()
