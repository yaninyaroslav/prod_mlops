import os
import mlflow
import uvicorn
import pandas as pd
import joblib as jb
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException


load_dotenv()

app = FastAPI()
# s3://arts/0/0987ee341cc347ffa2cb6fa90b678c07/artifacts/model/model.pkl
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://localhost:9000"


class Model:
    def __init__(self, model_name, model_stage):
        # self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")
        self.model = jb.load("/code/app/rfc.pkl")

    def predict(self, data):
        predictions = self.model.predict(data)
        return predictions


model = Model('RandomForestClassifier', 'Staging')


@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".csv"):
        print(file.filename)
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        print("End reading file")
        data = pd.read_csv(file.filename)
        print("Convert to pandas")
        os.remove(file.filename)
        print("Remove file")
        answer = model.predict(data)
        print("Prediction", answer)
        print(type(answer))
        return answer.tolist()
    else:
        raise HTTPException(status_code=400, detail="Invalid data. Only csv-data excepted")


# if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
#     exit(1)
