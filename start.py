import src
import click
from globals import RAW_TRAIN_DATA_PATH, RAW_PREDICTION_DATA_PATH, PROCESSED_Y_DATA_PATH, INTERIM_TRAIN_DATA_PATH, \
    INTERIM_PREDICTION_DATA_PATH, PROCESSED_DATA_TRAIN_PATH, SAVE_MODELS_PATH


@click.group()
def main():
    pass


# main.command(src.prepare_data([RAW_TRAIN_DATA_PATH,
#                                RAW_PREDICTION_DATA_PATH,
#                                PROCESSED_Y_DATA_PATH,
#                                INTERIM_TRAIN_DATA_PATH,
#                                INTERIM_PREDICTION_DATA_PATH]))

# main.command(src.new_features([RAW_TRAIN_DATA_PATH,
#                                RAW_PREDICTION_DATA_PATH,
#                                EXTERNAL_TRAIN_DATA_PATH,
#                                EXTERNAL_PREDICTION_DATA_PATH]))

# main.command(
#     src.stack_data([EXTERNAL_TRAIN_DATA_PATH,
#                     EXTERNAL_PREDICTION_DATA_PATH,
#                     INTERIM_TRAIN_DATA_PATH,
#                     INTERIM_PREDICTION_DATA_PATH,
#                     PROCESSED_DATA_TRAIN_PATH,
#                     PROCESSED_DATA_PREDICTION_PATH])
# )
main.command(
    src.train_rfc([PROCESSED_DATA_TRAIN_PATH,
                   PROCESSED_Y_DATA_PATH,
                   SAVE_MODELS_PATH])
)

# main.command(
#     src.predict_rfc([SAVE_MODELS_PATH,
#                      PROCESSED_DATA_PREDICTION_PATH,
#                      SAVE_PREDICTION_PATH])


if __name__ == "__main__":
    main()
