from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='New project for MLOps course',
    author='YaninYV',
    license='MIT',
)
